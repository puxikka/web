/** @file Role.java
 *  @brief Role
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
/** @brief package model
 */
package model;

/** @brief Libraries
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @brief Class Role
 */
@Entity
@Table(name = "role")
public class Role {

	/**
	 * @brief Attributes
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_ROLE")
	private int idRole;

	@NotEmpty
	@Column(name = "NOMBRE_ROLE")
	private String rolename;
	/**
	 * @brief Method for get the value of the idRole variable
	 * @return int
	 */
	public int getIdRole() {
		return idRole;
	}
	/**
	 * @brief Method for determine the idPedidos of the Role 
	 * @param idRole id of the Role
	 */
	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}
	/**
	 * @brief Method for get the value of the rolename variable
	 * @return String
	 */
	public String getRolename() {
		return rolename;
	}
	/**
	 * @brief Method for determine the name of the Role 
	 * @param rolename name of the Role
	 */
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	

}
