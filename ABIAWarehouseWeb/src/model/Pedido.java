/** @file Mensaje.java
 *  @brief Mensaje
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
/**
 * @brief Class Pedido
 */
@Entity
@Table(name = "pedidos")
public class Pedido {
	/**
	 * @brief Attributes
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_PEDIDOS")
	private int idPedidos;

	@Column(name = "USUARIO_ID")
	private int idUsuario;
	
	@Column(name = "ESTADO_ID")
	private int idEstado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_PEDIDO")
	private Date fechaPedido;
	
	@Column(name = "PRECIO_TOTAL")
	private int precioTotal;
	
	
	public Pedido() {
		
	}
	public Pedido(int idUsuario,int precioTotal) {
		super();
		this.idPedidos = 0;
		this.idUsuario = idUsuario;
		this.idEstado = 0;
		this.precioTotal = precioTotal;
		this.fechaPedido = new Date();
	}
	
	/**
	 * @brief Method for get the value of the idPedidos variable
	 * @return int
	 */
	public int getIdPedidos() {
		return idPedidos;
	}
	/**
	 * @brief Method for determine the idPedidos of the Pedido 
	 * @param idPedidos id of the pedido
	 */
	public void setIdPedidos(int idPedidos) {
		this.idPedidos = idPedidos;
	}
	/**
	 * @brief Method for get the value of the idUsuario variable
	 * @return int
	 */
	public int getIdUsuario() {
		return idUsuario;
	}
	/**
	 * @brief Method for determine the idUsuario of the Pedido 
	 * @param idUsuario id of the usuario
	 */
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	/**
	 * @brief Method for get the value of the idEstado variable
	 * @return int
	 */
	public int getIdEstado() {
		return idEstado;
	}
	/**
	 * @brief Method for determine the idPedidos of the Pedido 
	 * @param idPedidos id of the pedido
	 */
	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}
	/**
	 * @brief Method for get the value of the fechaPedido variable
	 * @return Date
	 */
	public Date getFechaPedido() {
		return fechaPedido;
	}
	/**
	 * @brief Method for determine the fechaPedido of the Pedido 
	 * @param fechaPedido date of the pedido
	 */
	public void setFechaPedido(Date fechaPedido) {
		this.fechaPedido = fechaPedido;
	}
	/**
	 * @brief Method for get the value of the precioTotal variable
	 * @return int
	 */
	public int getPrecioTotal() {
		return precioTotal;
	}
	/**
	 * @brief Method for determine the precioTotal of the Pedido 
	 * @param precioTotal prize of the pedido
	 */
	public void setPrecioTotal(int precioTotal) {
		this.precioTotal = precioTotal;
	}
}
