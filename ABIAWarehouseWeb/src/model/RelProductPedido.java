/** @file RelProductPedido.java
 *  @brief RelProductPedido
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package model;

/** @brief Libraries
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @brief Class RelProductPedido
 */
@Entity
@Table(name = "rel_product_pedido")
public class RelProductPedido {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_REL")
	private int idRel;
	
	@Column(name = "PEDIDOS_ID")
	private int idPedidos;


	@Column(name = "PRODUCT_ID")
	private int idProduct;
	
	@Column(name = "CANTIDAD_PEDIDO")
	private int cantidadPedido;
	
	public RelProductPedido() {
		super();
	}

	public RelProductPedido(int idPedidos, int idProduct, int cantidadPedido) {
		super();
		this.idRel = 0;
		this.idPedidos = idPedidos;
		this.idProduct = idProduct;
		this.cantidadPedido = cantidadPedido;
	}
	/**
	 * @brief Method for get the value of the idPedidos variable
	 * @return int
	 */
	public int getIdPedidos() {
		return idPedidos;
	}
	/**
	 * @brief Method for determine the id of the RelProductPedido 
	 * @param idPedidos id of the RelProductPedido
	 */
	public void setIdPedidos(int idPedidos) {
		this.idPedidos = idPedidos;
	}
	/**
	 * @brief Method for get the value of the idProduct variable
	 * @return int
	 */
	public int getIdProduct() {
		return idProduct;
	}
	/**
	 * @brief Method for determine the id of the Product
	 * @param idProduct id of the Product
	 */
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	/**
	 * @brief Method for get the value of the cantidadPedido variable
	 * @return int
	 */
	public int getCantidadPedido() {
		return cantidadPedido;
	}
	/**
	 * @brief Method for determine the quantity of the Product
	 * @param cantidadPedido quantity of the Product
	 */
	public void setCantidadPedido(int cantidadPedido) {
		this.cantidadPedido = cantidadPedido;
	}



	
}
