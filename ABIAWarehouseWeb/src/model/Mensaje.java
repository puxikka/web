/** @file Mensaje.java
 *  @brief Mensaje
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package model;
/** @brief Libraries
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.plexus.component.annotations.Component;
import org.springframework.stereotype.Service;
/**
 * @brief Class Mensaje
 */

public class Mensaje {

	/**
	 * @brief Attributes
	 */
	String mensaje;

	public Mensaje(String mensaje) {
		super();
		this.mensaje = mensaje;
	}
	public Mensaje() {
		
	}
	/**
	 * @brief Method for get the value of the mensaje variable
	 * @return String
	 */
	public String getMensaje() {
		return mensaje;
	}
	
	/**
	 * @brief Method for determine the id of the Order 
	 * @param mensaje text of the mensaje
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	
	

}
