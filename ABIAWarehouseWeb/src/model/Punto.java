/** @file Mensaje.java
 *  @brief Mensaje
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package model
 */
package model;

/** @brief Libraries
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @brief Class Punto
 */
@Entity
@Table(name = "punto")
public class Punto {

	/**
	 * @brief Attributes
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_PUNTO")
	private int idPunto;
	@NotNull
	@Column(name = "TIPO_PUNTO_ID")
	private int tipoPunto;
	@NotNull
	@Column(name = "USER_ID")
	private int userId;
	@NotNull
	@Column(name = "POS_X")
	private int posX;
	@NotNull
	@Column(name = "POS_Y")
	private int posY;
	@NotNull
	@Column(name = "NOMBRE_PUNTO")
	private String direccion;

	public Punto() {
		super();
	}

	public Punto(int tipoPunto, int userId, int posX, int posY, String direccion) {
		super();
		this.tipoPunto = tipoPunto;
		this.userId = userId;
		this.posX = posX;
		this.posY = posY;
		this.direccion = direccion;
	}
	
	/**
	 * @brief Method for get the value of the idPunto variable
	 * @return int
	 */
	public int getIdPunto() {
		return idPunto;
	}
	/**
	 * @brief Method for determine the id of the Punto 
	 * @param idPunto id of the Punto
	 */
	public void setIdPunto(int idPunto) {
		this.idPunto = idPunto;
	}
	/**
	 * @brief Method for get the value of the tipoPunto variable
	 * @return int
	 */
	public int getTipoPunto() {
		return tipoPunto;
	}
	/**
	 * @brief Method for determine the type of the Punto 
	 * @param tipoPunto type of the Punto
	 */
	public void setTipoPunto(int tipoPunto) {
		this.tipoPunto = tipoPunto;
	}
	/**
	 * @brief Method for get the value of the userId variable
	 * @return int
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * @brief Method for determine the id of the User 
	 * @param userId type of the User
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @brief Method for get the value of the posX variable
	 * @return int
	 */
	public int getPosX() {
		return posX;
	}
	/**
	 * @brief Method for determine the X position of the Punto 
	 * @param posX  X position of the Punto
	 */
	public void setPosX(int posX) {
		this.posX = posX;
	}
	/**
	 * @brief Method for get the value of the posY variable
	 * @return int
	 */
	public int getPosY() {
		return posY;
	}
	/**
	 * @brief Method for determine the Y position of the Punto 
	 * @param posY  Y position of the Punto
	 */
	public void setPosY(int posY) {
		this.posY = posY;
	}
	/**
	 * @brief Method for get the value of the direccion variable
	 * @return String
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @brief Method for determine the address of the Punto 
	 * @param direccion address of the Punto
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
}