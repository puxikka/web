/** @file product.java
 *  @brief product
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package model
 */
package model;

/** @brief Libraries
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Repository;

/**
 * @brief Class Product
 */
//@XmlRootElement
@Entity
@Table(name = "product")
public class Product {

	/**
	 * @brief Attributes
	 */
	//@XmlElement
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_PRODUCT")
	private int idProduct;

	//@XmlElement
	@NotNull
	@Column(name = "TIPO_PRODUCTO_ID")
	private int tipoProducto;

	//@XmlElement
	@NotNull
	@Column(name = "USUARIO_ID")
	private int usuarioId;

	//@XmlElement
	@NotNull
	@Column(name = "NOMBRE_PRODUCTO")
	private String nombreProducto;
	
	//@XmlElement
	@NotNull
	@Column(name = "CANTIDAD_PRODUCTO")
	private int cantidadProducto;
	
	//@XmlElement
	@NotNull
	@Column(name = "PRECIO_UNIDAD")
	private int precioUnidad;
	
	/**
	 * @brief Method for get the value of the idProduct variable
	 * @return int
	 */
	public int getIdProduct() {
		return idProduct;
	}
	/**
	 * @brief Method for determine the id of the Product 
	 * @param idProduct id of the Product
	 */
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	/**
	 * @brief Method for get the value of the tipoProducto variable
	 * @return int
	 */
	public int getTipoProducto() {
		return tipoProducto;
	}
	/**
	 * @brief Method for determine the type of the Product 
	 * @param tipoProducto type of the Product
	 */
	public void setTipoProducto(int tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	/**
	 * @brief Method for get the value of the usuarioId variable
	 * @return int
	 */
	public int getUsuarioId() {
		return usuarioId;
	}/**
	 * @brief Method for determine the id of the User 
	 * @param usuarioId id of the User
	 */
	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}
	/**
	 * @brief Method for get the value of the nombreProducto variable
	 * @return String
	 */
	public String getNombreProducto() {
		return nombreProducto;
	}
	/**
	 * @brief Method for determine the name of the Product 
	 * @param nombreProducto name of the Product
	 */
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	/**
	 * @brief Method for get the value of the cantidadProducto variable
	 * @return int
	 */
	public int getCantidadProducto() {
		return cantidadProducto;
	}
	/**
	 * @brief Method for determine the cantidadProducto of the Product 
	 * @param cantidadProducto quantity of the Product
	 */
	public void setCantidadProducto(int cantidadProducto) {
		this.cantidadProducto = cantidadProducto;
	}
	
	/**
	 * @brief Method for get the value of the precioUnidad variable
	 * @return int
	 */
	public int getPrecioUnidad() {
		return precioUnidad;
	}
	/**
	 * @brief Method for determine the precioUnidad of the Product 
	 * @param precioUnidad prize of the Product
	 */
	public void setPrecioUnidad(int precioUnidad) {
		this.precioUnidad = precioUnidad;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idProduct;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (idProduct != other.idProduct)
			return false;
		return true;
	}
	public Product() {
		
	}
	public Product(int idProduct, int tipoProducto, int usuarioId, String nombreProducto, int cantidadProducto,
			int precioUnidad) {
		super();
		this.idProduct = idProduct;
		this.tipoProducto = tipoProducto;
		this.usuarioId = usuarioId;
		this.nombreProducto = nombreProducto;
		this.cantidadProducto = cantidadProducto;
		this.precioUnidad = precioUnidad;
	}
}
