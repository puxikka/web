/** @file ProductType.java
 *  @brief ProductType
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package model;
/** @brief Libraries
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @brief Class ProductType
 */
@Entity
@Table(name = "tipo_producto")
public class ProductType {
	/**
	 * @brief Attributes
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_TIPO_PRODCUTO")
	private int idTipoProduct;

	@NotNull
	@Column(name = "NOMBRE_TIPO_PRODUCTO")
	private String nombreTipoProducto;

	/**
	 * @brief Method for get the value of the idTipoProduct variable
	 * @return int
	 */
	public int getIdTipoProduct() {
		return idTipoProduct;
	}
	/**
	 * @brief Method for determine the id of the ProductType 
	 * @param idTipoProduct id of the type
	 */
	public void setIdTipoProduct(int idTipoProduct) {
		this.idTipoProduct = idTipoProduct;
	}
	/**
	 * @brief Method for get the value of the nombreTipoProducto variable
	 * @return String
	 */
	public String getNombreTipoProducto() {
		return nombreTipoProducto;
	}
	/**
	 * @brief Method for determine the name of the ProductType 
	 * @param nombreTipoProducto name of the type
	 */
	public void setNombreTipoProducto(String nombreTipoProducto) {
		this.nombreTipoProducto = nombreTipoProducto;
	}
}
