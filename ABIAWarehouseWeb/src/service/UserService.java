/** @file UserService.java
 *  @brief UserService
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package service
 */package service;

/** @brief Libraries
 */
import model.User;

/**
 * @brief Class UserService
 */
public interface UserService {
	/**
	 * @brief Method that returns the user with the given username and password	
	 * @param username username
	 * @param password password
	 * @return User
	 */
	public abstract User validateUserCredential(String username, String password);
	/**
	 * @brief Method that save a new user in the database 	
	 * @param user a User
	 * @return boolean
	 */
	public abstract boolean registerUser(User user);
	/**
	 * @brief Method that get a User with a username
	 * @param username a String
	 * @return User
	 */
	public abstract User getUserWithUsername(String username);
}