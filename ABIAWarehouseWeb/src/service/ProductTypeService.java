/** @file ProductTypeService.java
 *  @brief ProductTypeService service
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package service
 */package service;

/** @brief Libraries
 */
import java.util.HashMap;

import model.ProductType;
/**
 * @brief interface ProductTypeService
 */
public interface ProductTypeService {
	/**
	 * @brief Method that returns the productType with the given id 	
	 * @param id idProductType
	 * @return ProductType
	 */
	public abstract ProductType validateProductType(int id);
	/**
	 * @brief Method that returns a HashMap of idProduct and ProductType 	
	 * @return HashMap<Integer, ProductType>
	 */
	public abstract HashMap<Integer, ProductType> getProductTypeMap();
}
