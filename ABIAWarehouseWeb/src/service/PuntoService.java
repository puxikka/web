/** @file PuntoService.java
 *  @brief PuntoService
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package service
 */package service;

/** @brief Libraries
 */
import model.Punto;
/**
 * @brief interface PuntoService
 */
public interface PuntoService {
	
	/**
	 * @brief Method that save a new user in the database 	
	 * @param user a User
	 * @return boolean
	 */
	public abstract boolean registerPunto(Punto punto);

}