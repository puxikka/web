/** @file PedidoDAOImpl.java
 *  @brief PedidoDAOImpl
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
/** @brief package service
 */package service;

/** @brief Libraries
 */
import java.util.ArrayList;
import java.util.List;

import model.Pedido;
import model.Product;

/**
 * @brief Class ProductService
 */
public interface ProductService {
	/**
	 * @brief Method that returns the product with the given id 	
	 * @param id idProduct
	 * @return Product
	 */
	public abstract Product validateProduct(int id);
	/**
	 * @brief Method that returns an array with all the products
	 * @return ArrayList<Product>
	 */
	public abstract List<Product> getProducts();
	/**
	 * @brief Method that deletes a product with that id
	 * @return ArrayList<Product>
	 */
	public abstract void deleteProduct(int productId);
	/**
	 * @brief Method that saves a product in de db 
	 * @param ArrayList<Product>
	 */
	public abstract void saveProduct(Product theProduct);
}
