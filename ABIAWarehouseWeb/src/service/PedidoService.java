/** @file PedidoService.java
 *  @brief PedidoService
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package service;
/** @brief Libraries
 */
import java.util.List;

import model.Pedido;
import model.Product;
/**
 * @brief interface PedidoService
 */

public interface PedidoService {
	/**
	 * @brief Method to get all the pedidos from the db
	 * @return List<Pedido>
	 */
	public abstract List<Pedido> getPedidos();
	/**
	 * @brief Method to add a Pedido
	 * @param pedido Pedido 
	 */
	public abstract void addPedido(Pedido pedido);
	
}
