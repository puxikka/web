/** @file ProductTypeServiceImpl.java
 *  @brief ProductTypeService implementation
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package service.impl
 */package service.impl;

/** @brief Libraries
 */
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.ProductTypeDAO;
import model.ProductType;
import service.ProductTypeService;

/**
 * @brief Class ProductTypeServiceImpl
 */
@Service("productType")
public class ProductTypeServiceImpl implements ProductTypeService{

	/**
	 * @brief Attributes
	 */
	@Autowired
	private ProductTypeDAO productTypeDAO;
	/**
	 * @brief Method for determine the value of the variable productTypeDAO 
	 * @param ProductTypeDAO productTypeDAO
	 */
	public void productTypeDAO(ProductTypeDAO productTypeDAO) {
		this.productTypeDAO = productTypeDAO;
	}
	/**
	 * @brief Method for get the value of the productTypeDAO variable
	 * @return ProductTypeDAO
	 */
	public ProductTypeDAO getProductTypeDAO() {
		return productTypeDAO;
	}

	@Override
	public ProductType validateProductType(int id) {
		ProductType productType = getProductTypeDAO().getNameById(id);
		return productType;
	}

	@Override
	public HashMap<Integer, ProductType> getProductTypeMap() {
		// TODO Auto-generated method stub
		HashMap<Integer, ProductType> hmap=getProductTypeDAO().getProductTypeMap();
		return hmap;
	}
}
