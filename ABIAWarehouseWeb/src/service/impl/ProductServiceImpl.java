/** @file ProductServiceImpl.java
 *  @brief ProductService implementation
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package service.impl
 */package service.impl;

/** @brief Libraries
 */
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.ProductDAO;
import model.Pedido;
import model.Product;
import service.ProductService;

/**
 * @brief Class ProductServiceImpl
 */
@Service("product")
public class ProductServiceImpl implements ProductService {

	/**
	 * @brief Attributes
	 */
	@Autowired
	private ProductDAO productDAO;

	public void setProductDAO(ProductDAO productDAO) {
		this.productDAO = productDAO;
	}
	/**
	 * @brief Method for get the value of the productDAO variable
	 * @return ProductDAO
	 */
	public ProductDAO getProductDAO() {
		return productDAO;
	}

	@Override
	public Product validateProduct(int id) {
		Product product = getProductDAO().getProductTypeById(id);
		return product;
	}

	@Override
	public List<Product> getProducts() {
		List<Product> product = getProductDAO().getAllProducts();
		return product;
	}

	@Override
	public void deleteProduct(int productId) {
		
		getProductDAO().deleteProduct(productId);
		
	}

	@Override
	public void saveProduct(Product theProduct) {
		
		getProductDAO().saveProduct(theProduct);
		
	}


}
