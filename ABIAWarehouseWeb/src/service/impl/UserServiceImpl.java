/** @file UserServiceImpl.java
 *  @brief UserService implementation
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package service.impl
 */package service.impl;

/** @brief Libraries
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.UserDAO;
import model.User;
import service.UserService;

/**
 * @brief Class UserServiceImpl
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	/**
	 * @brief Attributes
	 */
	@Autowired
	private UserDAO userDAO;
	
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	public UserDAO getUserDAO() {
		return userDAO;
	}
	
	@Override
	public User validateUserCredential(String username, String password) {
		User user = getUserDAO().getUserDetailsByUsernameAndPassword(username, password);
		
		return user;
	}
	
	@Override
	public User getUserWithUsername(String username) {
		User user = getUserDAO().getUserWithName(username);
		
		return user;
	}
	
	
	@Override
	public boolean registerUser(User user) {
		boolean isRegister=false;
		boolean saveStudent = getUserDAO().addUser(user);
		if(saveStudent)
			isRegister=true;
		return isRegister;
	}
}
