/** @file RoleServiceImpl.java
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package service.impl
 */package service.impl;

/** @brief Libraries
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.RoleDAO;
import model.Role;
import service.RoleService;


/**
 * @brief Class RoleServiceImpl
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {

	/**
	 * @brief Attributes
	 */
	@Autowired
	private RoleDAO roleDAO;
	
	public void setRoleDAO(RoleDAO roleDAO) {
		this.roleDAO = roleDAO;
	}
	
	public RoleDAO getRoleDAO() {
		return roleDAO;
	}
	
	

	@Override
	public Role validateRole(int id) {
		Role role = getRoleDAO().getRoleDetailsById(id);
		return role;
	}
}
