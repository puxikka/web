/** @file PuntoServiceImpl.java
 *  @brief PuntoServiceImpl
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package service.impl
 */package service.impl;

/** @brief Libraries
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.PuntoDAO;
import dao.UserDAO;
import model.Punto;
import model.User;
import service.PuntoService;
import service.UserService;

/**
 * @brief Class PuntoServiceImpl
 */
@Service("puntoService")
public class PuntoServiceImpl implements PuntoService {

	/**
	 * @brief Attributes
	 */
	@Autowired
	private PuntoDAO puntoDAO;
	

	/**
	 * @brief Method for gets the value of the variable productTypeDAO 
	 * @return PuntoDAO puntoDAO
	 */
	public PuntoDAO getPuntoDAO() {
		return puntoDAO;
	}
	/**
	 * @brief Method for determine the value of the variable productTypeDAO 
	 * @param PuntoDAO puntoDAO
	 */
	public void setPuntoDAO(PuntoDAO puntoDAO) {
		this.puntoDAO = puntoDAO;
	}

	@Override
	public boolean registerPunto(Punto punto) {
		boolean isRegister=false;
		boolean savePunto = getPuntoDAO().addPunto(punto);
		if(savePunto)
			isRegister=true;
		return isRegister;
	}
}
