
/** @file PedidoServiceImp.java
 *  @brief PedidoServiceImp
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package service.impl;
/** @brief Libraries
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.PedidoDAO;
import dao.ProductDAO;
import model.Pedido;
import model.Product;
import service.PedidoService;

/**
 * @brief Class PedidoServiceImp
 */
@Service("pedido")
public class PedidoServiceImp implements PedidoService {

	@Autowired
	private PedidoDAO pedidoDAO;

	/**
	 * @brief Method for set the value of the pedidoDAO variable
	 * @param PedidoDAO productDAO
	 */
	public void setPedidoDAO(PedidoDAO productDAO) {
		this.pedidoDAO = productDAO;
	}
	/**
	 * @brief Method for get the value of the pedidoDAO variable
	 * @return PedidoDAO
	 */
	public PedidoDAO getPedidoDAO() {
		return pedidoDAO;
	}
	@Override
	public List<Pedido> getPedidos() {
		List<Pedido> product = getPedidoDAO().getAllPedidos();
		return product;
	}

	@Override
	public void addPedido(Pedido pedido) {
		pedidoDAO.aņadirPedido(pedido);
	}

	
	

}
