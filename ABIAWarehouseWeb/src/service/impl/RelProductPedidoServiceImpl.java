/** @file RelProductPedidoServiceImpl.java
 *  @brief RelProductPedidoServiceImpl 
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package service.impl
 */package service.impl;

import java.util.List;

/** @brief Libraries
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import dao.RelProductPedidoDAO;
import model.Product;
import model.RelProductPedido;
import service.RelProductPedidoService;

/**
 * @brief Class RelProductPedidoServiceImpl
 */
@Service("rel")
public class RelProductPedidoServiceImpl implements RelProductPedidoService {

	/**
	 * @brief Attributes
	 */
	@Autowired
	private RelProductPedidoDAO relDAO;


	@Override
	public void saveRelProductPedido(model.RelProductPedido rel) {
		relDAO.saveRelProductPedido(rel);
		
	}


	@Override
	public List<RelProductPedido> getRels() {
		List<RelProductPedido> product = relDAO.getAllRel();
		return product;
	}


}
