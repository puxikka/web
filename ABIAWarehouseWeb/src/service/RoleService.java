/** @file RoleService.java
 *  @brief RoleService
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package service
 */package service;

/** @brief Libraries
 */
import model.Role;
import model.User;

/**
 * @brief interface RoleService
 */
public interface RoleService {
	/**
	 * @brief Method that returns the role with the given id 
	 * @param id idRole	
	 * @return Role
	 */
	public abstract Role validateRole(int id);

}