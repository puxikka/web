/** @file RelProductPedidoService.java
 *  @brief RelProductPedidoService service
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package service
 */package service;

/** @brief Libraries
 */
import java.util.ArrayList;
import java.util.List;

import model.Pedido;
import model.Product;
import model.RelProductPedido;

/**
 * @brief interface ProductService
 */
public interface RelProductPedidoService {
	/**
	 * @brief Method that save a new relation id Product and Pedido in the database 	
	 * @param user a User
	 * @return boolean
	 */
	public abstract void saveRelProductPedido(RelProductPedido rel);
	/**
	 * @brief Method that gets all the Relations between Productos and Pedido
	 * @return List<RelProductPedido>
	 */
	public abstract List<RelProductPedido> getRels();
}
