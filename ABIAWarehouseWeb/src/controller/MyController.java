/** @file MyController.java
 *  @brief Controller
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package controller
 */
package controller;
/** @brief Libraries
 */
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import model.Pedido;
import model.Product;
import model.ProductType;
import model.Punto;
import model.User;
import model.UserCredential;
import service.PedidoService;
import service.ProductService;
import service.ProductTypeService;
import service.PuntoService;
import service.RoleService;
import service.UserService;

/**
 * @brief Class MyController
 */
@Controller
@Scope("session")
public class MyController {

	@Autowired
	private PuntoService puntoService;

	/**
	 * @brief Method for determine the userService
	 * @param userService userService
	 */
	public void setPuntoService(PuntoService puntoService) {
		this.puntoService = puntoService;
	}

	/**
	 * @brief Method for get the value of the userService variable
	 * @return UserService
	 */
	public PuntoService getPuntoService() {
		return puntoService;
	}
	@Autowired
	private PedidoService pedidoService;
	
	public void setPedidoService(PedidoService pedidoService) {
		this.pedidoService = pedidoService;
	}
	
	public PedidoService getPedidoService() {
		return pedidoService;
	}
	/**
	 * @brief Attributes
	 */
	@Autowired
	private UserService userService;

	/**
	 * @brief Method for determine the userService
	 * @param userService userService
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * @brief Method for get the value of the userService variable
	 * @return UserService
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * @brief Attributes
	 */
	@Autowired
	private RoleService roleService;

	/**
	 * @brief Method for get the value of the roleService variable
	 * @return RoleService
	 */
	public RoleService getRoleService() {
		return roleService;
	}

	/**
	 * @brief Method for determine the roleService
	 * @param roleService roleService
	 */
	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	/**
	 * @brief Attributes
	 */
	@Autowired
	private ProductService productService;

	/**
	 * @brief Method for get the value of the productService variable
	 * @return ProductService
	 */
	public ProductService getProductService() {
		return productService;
	}

	/**
	 * @brief Method for determine the productService
	 * @param productService productService
	 */
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	/**
	 * @brief Attributes
	 */
	@Autowired
	private ProductTypeService productTypeService;

	/**
	 * @brief Method for get the value of the productTypeService variable
	 * @return ProductTypeService
	 */
	public ProductTypeService getProductTypeService() {
		return productTypeService;
	}

	/**
	 * @brief Method for determine the productTypeService
	 * @param productTypeService productTypeService
	 */
	public void setProductTypeService(ProductTypeService productTypeService) {
		this.productTypeService = productTypeService;
	}


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String homePage() {
		return "home";
	}

	@RequestMapping(value = "logOff", method = RequestMethod.GET)
	public String logOff(HttpSession session) {
		session.setAttribute("user", null);
		return "home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(Model model) {
		model.addAttribute("userCredential", new UserCredential());
		return "login";
	}
	
	@RequestMapping(value = "/historicoUsuario", method = RequestMethod.GET)
	public String historicoUsuario(Model model, HttpSession session) {
	
		if (session.getAttribute("user") != null) {
			User user = (User) session.getAttribute("user");

			List<Pedido> pr = getPedidoService().getPedidos();
			User user2 = getUserService().getUserWithUsername(user.getUsername());
			System.out.println("estas conectado con : "+user2.getNombre_usuario());
			List<Pedido> listaPedidos = new ArrayList<Pedido>();
			for(Pedido p : pr) {
				if(p.getIdUsuario() == user2.getIdUser()) {
					listaPedidos.add(p);
				}
			}
			if(!listaPedidos.isEmpty()) model.addAttribute("pedidos", listaPedidos);
		} 
			
		return "historicoUsuario";
	}


	@RequestMapping(value = "/loginSuccess", method = RequestMethod.POST)
	public ModelAndView loginSuccess(@Valid @ModelAttribute("userCredential") UserCredential userCredential,
			BindingResult bindingResult, HttpSession session) {
		
		
		if (bindingResult.hasErrors()) {
			return new ModelAndView("login");
		}

		ModelAndView modelAndView = new ModelAndView("welcomeCustomer");
		
		User user = getUserService().getUserWithUsername(userCredential.getUsername());

		if (user != null) {
			session.setAttribute("user", user);

			if (session.getAttribute("user") != null) {
				User user2 = (User) session.getAttribute("user");
			}

			String passwordToHash = userCredential.getPassword();
			String securePassword = getSecurePassword(passwordToHash, user.getSalt().getBytes());

			if(securePassword.equals(user.getPassword())) {
				int role = user.getIdRole();
				if (role == 3) {
					modelAndView = new ModelAndView("welcomeManager");
				} else if (role == 2) {
					modelAndView = new ModelAndView("welcomeOperator");
				}
				modelAndView.addObject("user", user);

			} else {
				modelAndView = new ModelAndView("notFound");
			}
		}
		return modelAndView;
	}
	@RequestMapping(value = "/registerSuccess", method = RequestMethod.POST)
	public ModelAndView registerSuccess(HttpSession session,HttpServletRequest request) throws NoSuchAlgorithmException, NoSuchProviderException{
	
		ModelAndView modelAndView = new ModelAndView("home");
		//-------------User----------------
		try {
		User user = new User(Integer.parseInt(request.getParameter("idRole")),request.getParameter("name"),request.getParameter("nombre_usuario"), request.getParameter("password"));
		
		String passwordToHash = user.getPassword();
		byte[] salt = getSalt();
		user.setSalt(salt.toString());
		String securePassword = getSecurePassword(passwordToHash, user.getSalt().getBytes());
		user.setPassword(securePassword);
		getUserService().registerUser(user);
		
		
		int idPunto = user.getIdRole();
		if(idPunto == 2)idPunto = 0;
		else idPunto = 1;
		Punto punto = new Punto(idPunto,user.getIdUser(),Integer.parseInt(request.getParameter("posX")),
				Integer.parseInt(request.getParameter("posY")),request.getParameter("direccion"));
		getPuntoService().registerPunto(punto);
		session.setAttribute("user", user);
		return modelAndView;
		
		}catch(Exception e){
			modelAndView = new ModelAndView("error");
			e.printStackTrace();
			return modelAndView;
		}

		
	}

	

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String registerPage(Model model) {
		
		return "register";
	}

	@RequestMapping(value = "/registerUser", method = RequestMethod.GET)
	public String registerUserPage(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("punto", new Punto());
		return "registerUser";
	}

	@ModelAttribute
	public void headerMessage(Model model) {
		model.addAttribute("headerMessage", "Welcome");

	}

	@RequestMapping(value = "/knowmore", method = RequestMethod.GET)
	public String knowmorePage(Model model) {
		model.addAttribute("userCredential", new UserCredential());
	
		return "knowmore";
	}
	@RequestMapping(value = "/pruebas", method = RequestMethod.GET)
	public String pruebas(Model model) {
		
		return "pruebas";
	}

	@RequestMapping(value = "/selectProducts", method = RequestMethod.POST)
	public void validateSymbol(WebRequest webRequest, Model model, HttpServletRequest request,
			HttpServletResponse response, @RequestParam String[] data, HttpSession session) throws IOException {


	}

	@RequestMapping(value = "/productSelection", method = RequestMethod.GET)
	public String productSelection(Model model) {
		HashMap<Integer, ProductType> hmap = getProductTypeService().getProductTypeMap();

		return "productSelection";
	}

	/**
	 * @brief Method that Get complete hashed password
	 * @return String
	 */
	private static String getSecurePassword(String passwordToHash, byte[] salt)
	{
		String generatedPassword = null;
		try {
			// Create MessageDigest instance for MD5
			MessageDigest md = MessageDigest.getInstance("MD5");
			//Add password bytes to digest
			md.update(salt);
			//Get the hash's bytes
			byte[] bytes = md.digest(passwordToHash.getBytes());
			//This bytes[] has bytes in decimal format;
			//Convert it to hexadecimal format
			StringBuilder sb = new StringBuilder();
			for(int i=0; i< bytes.length ;i++)
			{
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			//Get complete hashed password in hex format
			generatedPassword = sb.toString();
		}
		catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}

	/**
	 * @brief Method that generates a random Salt
	 * @return byte[] 
	 */
	private static byte[] getSalt() throws NoSuchAlgorithmException, NoSuchProviderException
	{
		//Always use a SecureRandom generator
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
		//Create array for salt
		byte[] salt = new byte[16];
		//Get a random salt
		sr.nextBytes(salt);
		//return salt
		return salt;
	}

}
