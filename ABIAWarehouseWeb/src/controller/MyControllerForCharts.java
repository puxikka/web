/** @file MyControllerForCharts.java
 *  @brief Controller
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package controller
 */
package controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.google.gson.Gson;

import Validator.ValidationUtils;
import model.Mensaje;
import model.Pedido;
import model.Product;
import model.RelProductPedido;
import model.User;
import restService.PedidoServiceRest;
import restService.ProductServiceRest;
import restService.RelProductoPedidoServiceRest;
import schemas.Schemas;
import service.ProductService;

/**
 * @brief Class MyControllerForCharts
 */

@Controller
@Scope("session")
public class MyControllerForCharts {
	/**
	 * @brief Attributes
	 */
	
	@Autowired
	private RelProductoPedidoServiceRest relServiceRest;

	@Autowired
	private ProductServiceRest productServiceRest;

	@Autowired
	private PedidoServiceRest pedidoServiceRest;
	
	@Autowired
	private ProductService productService;

	/**
	 * @brief Method for get the value of the productService variable
	 * @return ProductService
	 */
	public ProductService getProductService() {
		return productService;
	}

	@RequestMapping(value = "/createCesta", method = RequestMethod.POST)
	public String createCesta(Model model, HttpSession session, HttpServletRequest request,
			@RequestParam("cestas") String cesta) {

		List<Product> pr = new ArrayList<Product>();
		JSONArray jsonArr = new JSONArray(cesta);

		for (int i = 0; i < jsonArr.length(); i++) {
			JSONObject jsonObj = jsonArr.getJSONObject(i);

			Product p = new Product();
			System.out.println(i + " -> " + jsonObj);

			if (jsonObj.getInt("cantidadProducto") > 0) {

				p.setCantidadProducto(jsonObj.getInt("cantidadProducto"));
				p.setIdProduct(jsonObj.getInt("idProduct"));
				p.setNombreProducto(jsonObj.getString("nombreProducto"));
				p.setPrecioUnidad(jsonObj.getInt("precioUnidad"));
				p.setTipoProducto(jsonObj.getInt("tipoProducto"));
				p.setUsuarioId(jsonObj.getInt("usuarioId"));

				pr.add(p);
			}

		}

		model.addAttribute("products", pr);
		String json = new Gson().toJson(pr);
		request.setAttribute("productos", json);

		return "createCesta";
	}

	@RequestMapping(value = "/comprar", method = RequestMethod.POST)
	public String comprar(Model model, @RequestParam(value = "cesta") String cestas, HttpServletRequest request,
			HttpSession session) throws ProcessingException, IOException {

		List<Product> listaActualizada = new ArrayList<Product>();
		System.out.println("comprar : " + cestas);
		Schemas schema = new Schemas();
		List<Integer> listaCantidades = new ArrayList<Integer>();
		int sumaCompra = 0;
		if (ValidationUtils.isJsonValid(schema.getComprarProductoSchema(), cestas)) {

			JSONArray jsonArr = new JSONArray(cestas);

			for (int i = 0; i < jsonArr.length(); i++) {
				JSONObject jsonObj = jsonArr.getJSONObject(i);
				int cantidad = productServiceRest.getProduct(jsonObj.getInt("idProduct")).getCantidadProducto();
				Product p = new Product();
				if (cantidad < jsonObj.getInt("cantidadProducto"))
					return "error";
				sumaCompra = sumaCompra + (jsonObj.getInt("cantidadProducto") * jsonObj.getInt("precioUnidad"));
				p.setCantidadProducto(cantidad - jsonObj.getInt("cantidadProducto"));
				listaCantidades.add(jsonObj.getInt("cantidadProducto"));
				p.setIdProduct(jsonObj.getInt("idProduct"));
				p.setNombreProducto(jsonObj.getString("nombreProducto"));
				p.setPrecioUnidad(jsonObj.getInt("precioUnidad"));
				p.setTipoProducto(jsonObj.getInt("tipoProducto"));
				p.setUsuarioId(jsonObj.getInt("usuarioId"));
				listaActualizada.add(p);

			}
			for (Product p : listaActualizada) {
				String json = new Gson().toJson(p);
				if (ValidationUtils.isJsonValid(schema.getUpdateProductSchema(), json)) {
					productServiceRest.updateProductWithJson(json);
				}else return "error";

			}

			if (session.getAttribute("user") != null) {
				User user = (User) session.getAttribute("user");
				Pedido pedido = new Pedido(user.getIdUser(), sumaCompra);
				pedidoServiceRest.savePedido(pedido);
				List<Pedido> listaPedidos = pedidoServiceRest.getPedidos();
				int pedidoId = listaPedidos.get(listaPedidos.size() - 1).getIdPedidos();

				for (int i = 0; i < listaActualizada.size(); i++) {
					Product p = listaActualizada.get(i);
					RelProductPedido rel = new RelProductPedido(pedidoId, p.getIdProduct(), listaCantidades.get(i));
					relServiceRest.saveRel(rel);
				}

				List<Product> pr = productServiceRest.getProducts();
				List<Product> listaProductos = new ArrayList<Product>();

				for (Product p : pr) {
					if (p.getUsuarioId() == user.getIdUser()) {
						listaProductos.add(p);
					}
				}
			}

			System.out.println("Compra Valida!");
		} else {
			System.out.println("NOT valid!");

		}

		List<Product> pr = productServiceRest.getProducts();
		model.addAttribute("products", pr);
		String json = new Gson().toJson(pr);
		request.setAttribute("productos", json);

		return "verProductos";
	}

	@RequestMapping(value = "/verProductos", method = RequestMethod.GET)
	public String verProductos(Model model, HttpServletRequest request, RedirectAttributes rm) throws IOException {

		List<Product> pr = productServiceRest.getProducts();
		model.addAttribute("products", pr);
		String json = new Gson().toJson(pr);
		request.setAttribute("productos", json);

		rm.addFlashAttribute("cestas", "cestas");

		return "verProductos";
	}

	@GetMapping("/orderHistoryAll")
	public String orderHistoryAll(Model model, HttpSession session, HttpServletRequest request, RedirectAttributes rm) {

		List<Product> pr = getProductService().getProducts();
		List<Product> listaProductos = new ArrayList<Product>();
		if (session.getAttribute("user") != null) {
			User user = (User) session.getAttribute("user");

			for (Product p : pr) {
				if (p.getUsuarioId() == user.getIdUser()) {
					listaProductos.add(p);
				}
			}
		}
		if (!listaProductos.isEmpty()) {
			model.addAttribute("products", listaProductos);
			String json = new Gson().toJson(listaProductos);
			request.setAttribute("productos", json);
		}

		return "orderHistoryAll";
	}

	@RequestMapping(value = "/eliminarOrderHistoryAll", method = RequestMethod.POST)
	public String eliminarOrderHistoryAll(Model model, HttpSession session, HttpServletRequest request,
			@RequestParam(value = "cesta") String cesta, RedirectAttributes rm) {

		JSONArray jsonArr = new JSONArray(cesta);

		List<Product> pr = new ArrayList<Product>();
		for (int i = 0; i < jsonArr.length(); i++) {
			JSONObject jsonObj = jsonArr.getJSONObject(i);

			Product p = new Product();
			System.out.println(i + " -> " + jsonObj);

			if (!jsonObj.getString("nombreProducto").equals("eliminar")) {

				p.setCantidadProducto(jsonObj.getInt("cantidadProducto"));
				p.setIdProduct(jsonObj.getInt("idProduct"));
				p.setNombreProducto(jsonObj.getString("nombreProducto"));
				p.setPrecioUnidad(jsonObj.getInt("precioUnidad"));
				p.setTipoProducto(jsonObj.getInt("tipoProducto"));
				p.setUsuarioId(jsonObj.getInt("usuarioId"));

				pr.add(p);
			}

		}

		model.addAttribute("products", pr);
		String jsonString = new Gson().toJson(pr);
		request.setAttribute("productos", jsonString);
		rm.addFlashAttribute("cesta", "cesta");
		return "orderHistoryAll";
	}

	@RequestMapping(value = "/saveOrderHistoryAll", method = RequestMethod.POST)
	public String saveOrderHistoryAll(Model model, HttpSession session, HttpServletRequest request,
			@RequestParam(value = "cesta") String cesta, RedirectAttributes rm)
			throws ProcessingException, IOException {

		System.out.println("cambios en la tienda: " + cesta);
		Schemas schema = new Schemas();

		if (ValidationUtils.isJsonValid(schema.getEditarProductoSchema(), cesta)) {

			List<Product> listaActualizada = new ArrayList<Product>();
			JSONArray jsonArr = new JSONArray(cesta);

			for (int i = 0; i < jsonArr.length(); i++) {
				JSONObject jsonObj = jsonArr.getJSONObject(i);

				Product p = new Product();

				p.setCantidadProducto(jsonObj.getInt("cantidadProducto"));
				p.setIdProduct(jsonObj.getInt("idProduct"));
				p.setNombreProducto(jsonObj.getString("nombreProducto"));
				p.setPrecioUnidad(jsonObj.getInt("precioUnidad"));
				p.setTipoProducto(jsonObj.getInt("tipoProducto"));
				p.setUsuarioId(jsonObj.getInt("usuarioId"));
				listaActualizada.add(p);
			}
			for (Product p : listaActualizada) {
				productServiceRest.saveProduct(p);
			}

			List<Product> pr = productServiceRest.getProducts();
			List<Product> listaProductos = new ArrayList<Product>();
			if (session.getAttribute("user") != null) {
				User user = (User) session.getAttribute("user");

				for (Product p : pr) {
					if (p.getUsuarioId() == user.getIdUser()) {
						listaProductos.add(p);
					}
				}
			}
			List<Product> listOne = listaProductos;
			List<Product> listTwo = listaActualizada;

			List<Product> uniqueElementsFromBothList = new ArrayList<>();

			// Unique element from listOne
			uniqueElementsFromBothList
					.addAll(listOne.stream().filter(str -> !listTwo.contains(str)).collect(Collectors.toList()));

			for (Product p : uniqueElementsFromBothList) {

				System.out.println("Producto a borrar: " + p.getNombreProducto());
				productServiceRest.deleteProduct(p.getIdProduct());
			}

			System.out.println("Valid!");

		} else {
			System.out.println("NOT valid!");
			return "error";
		}

		return "valido";
	}

	@RequestMapping(value = "/logistic", method = RequestMethod.GET)
	public String logistic(Model model) {

		model.addAttribute("product", new Product());

		return "logistic";
	}

	@RequestMapping(value = "/vehicleInfo", method = RequestMethod.POST)
	public String saveProduct(Model model, @ModelAttribute("product") Product p, HttpSession session)
			throws ProcessingException, IOException {

		System.out.println(p.getNombreProducto());
		if (session.getAttribute("user") != null) {
			User user = (User) session.getAttribute("user");
			p.setUsuarioId(user.getIdUser());
		}

		String jsonString = new Gson().toJson(p);
		System.out.println(jsonString);

		Schemas schema = new Schemas();

		if (ValidationUtils.isJsonValid(schema.getAddProductSchema(), jsonString)) {
			System.out.println("Valid!");

			productServiceRest.saveProductWithJson(jsonString);

		} else {
			System.out.println("NOT valid!");
			return "error";
		}

		return "valido";
	}

	@RequestMapping(value = "/workstationInfo", method = RequestMethod.GET)
	public String mostrarCatalogo(Model model, HttpSession session) {

		model.addAttribute("jsonString", new Mensaje());

		return "workstationInfo";
	}

	@RequestMapping(value = "/aņadirCatalogo", method = RequestMethod.POST)
	public String aņadirCatalogo(Model model, @ModelAttribute("jsonString") Mensaje m, HttpSession session)
			throws ProcessingException, IOException {

		int idUser = 0;
		if (session.getAttribute("user") != null) {
			User user = (User) session.getAttribute("user");
			idUser = user.getIdUser();
		}

		try {
			System.out.println(m.getMensaje());

			String addCatalogoSchema = "{\r\n" + "  \"type\": \"array\",\r\n" + "  \"items\": {\r\n"
					+ "    \"type\": \"object\",\r\n" + "    \"properties\": {\r\n" + "      \"usuarioId\": {\r\n"
					+ "          \"type\": \"integer\",\r\n" + "		  \"minimum\": " + idUser + ",\r\n"
					+ "		  \"maximum\": " + idUser + "\r\n" + "        },\r\n" + "      \"tipoProducto\": {\r\n"
					+ "          \"type\": \"integer\",\r\n" + "		  \"maximum\": 4\r\n" + "        },\r\n"
					+ "        \"precioUnidad\": {\r\n" + "          \"type\": \"integer\",\r\n"
					+ "		  \"minimum\": 0\r\n" + "        },\r\n" + "        \"cantidadProducto\": {\r\n"
					+ "          \"type\": \"integer\",\r\n" + "		  \"minimum\": 0\r\n" + "        },\r\n"
					+ "        \"nombreProducto\": {\r\n" + "          \"type\": \"string\"\r\n" + "        }\r\n"
					+ "      },\r\n" + "      \"required\": [\r\n" + "        \"usuarioId\",\r\n"
					+ "        \"tipoProducto\",\r\n" + "        \"precioUnidad\",\r\n"
					+ "        \"cantidadProducto\",\r\n" + "        \"nombreProducto\"\r\n" + "    ]\r\n" + "  }\r\n"
					+ "}";
			if (ValidationUtils.isJsonValid(addCatalogoSchema, m.getMensaje())) {

				productServiceRest.saveCatalogotWithJson(m.getMensaje());

				System.out.println("Valid!");
			} else {

				System.out.println("NOT valid!");
				return "error";
			}
		} catch (Exception e) {

			e.printStackTrace();
			return "error";
		}

		return "valido";
	}

}
