/** @file ProductServiceRest.java
 *  @brief ProductServiceRest
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package restService;
/** @brief Libraries
 */
import java.util.List;

import model.Product;
/**
 * @brief interface ProductServiceRest
 */
public interface ProductServiceRest {

	/**
	 * @brief Method  to get all the products 
	 * @return List<Product>
	 */
	public List<Product> getProducts();
	/**
	 * @brief Method  to save a product
	 * @param theProduct  of Product
	 * @return List<Product>
	 */
	public void saveProduct (Product theProduct);
	/**
	 * @brief Method  to save a product with json
	 * @param jsonString  of String
	 */
	public void saveProductWithJson(String jsonString);
	/**
	 * @brief Method  to save a catalog of produscts with json
	 * @param jsonString  of String
	 */
	public void saveCatalogotWithJson(String jsonString);
	/**
	 * @brief Method  to get a Product with the id of the Product
	 * @param theId  of Product
	 * @return Product
	 */
	public Product getProduct(int theId);
	/**
	 * @brief Method  to delete a Product with the id of the Product
	 * @param theId  of Product
	 */
	public void deleteProduct(int theId);
	/**
	 * @brief Method  update a Product with a Json
	 * @param jsonString  of String
	 */
	public void updateProductWithJson(String jsonString);
	
	
}
