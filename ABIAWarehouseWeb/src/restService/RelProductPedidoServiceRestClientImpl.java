/** @file RelProductPedidoServiceRestClientImpl.java
 *  @brief MenRelProductPedidoServiceRestClientImplsaje
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package restService;

/** @brief Libraries
 */
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import model.Product;
import model.RelProductPedido;

/**
 * @brief Class RelProductPedidoServiceRestClientImpl
 */

@Service("relRest")
public class RelProductPedidoServiceRestClientImpl implements RelProductoPedidoServiceRest {

	public RelProductPedidoServiceRestClientImpl() {

	}

	/**
	 * @brief Attributes
	 */
	private RestTemplate restTemplate;

	private String crmRestUrl;

	private Logger logger = Logger.getLogger(getClass().getName());

	@Autowired
	public RelProductPedidoServiceRestClientImpl(RestTemplate theRestTemplate,
			@Value("http://localhost:8080/SpringMVCFormValidationPruebas/api/rel") String theUrl) {
		restTemplate = theRestTemplate;
		crmRestUrl = theUrl;

		logger.info("Loaded property:  crm.rest.url=" + crmRestUrl);
	}

	@Override
	public void saveRel(RelProductPedido rel) {
		logger.info("in saveRel(): Calling REST API " + crmRestUrl);

		// add rel
		restTemplate.postForEntity(crmRestUrl, rel, String.class);

		logger.info("in saveRel(): success POST");
	}

	@Override
	public List<RelProductPedido> getAllRelaciones() {
		logger.info("in getRelaciones(): Calling REST API " + crmRestUrl);

		ResponseEntity<List<RelProductPedido>> responseEntity = restTemplate.exchange(crmRestUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<RelProductPedido>>() {
				});

		List<RelProductPedido> relaciones = responseEntity.getBody();

		logger.info("in getRelaciones(): Relacion" + relaciones);

		return relaciones;
	}

}
