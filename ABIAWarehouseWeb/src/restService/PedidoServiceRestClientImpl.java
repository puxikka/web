/** @file PedidoServiceRestClientImpl.java
 *  @brief PedidoServiceRestClientImpl
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package restService;

/** @brief Libraries
 */
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import model.Pedido;
import model.RelProductPedido;

/**
 * @brief Class PedidoServiceRestClientImpl
 */

@Service("pedidoRest")
public class PedidoServiceRestClientImpl implements PedidoServiceRest {

	/**
	 * @brief Attributes
	 */
	public PedidoServiceRestClientImpl() {

	}

	private RestTemplate restTemplate;

	private String crmRestUrl;

	private Logger logger = Logger.getLogger(getClass().getName());

	@Autowired
	public PedidoServiceRestClientImpl(RestTemplate theRestTemplate,
			@Value("http://localhost:8080/SpringMVCFormValidationPruebas/api/pedidos") String theUrl) {
		restTemplate = theRestTemplate;
		crmRestUrl = theUrl;

		logger.info("Loaded property:  crm.rest.url=" + crmRestUrl);
	}

	@Override
	public void savePedido(Pedido thePedido) {
		logger.info("in saveCustomer(): Calling REST API " + crmRestUrl);

		int pedidotId = thePedido.getIdPedidos();

		if (pedidotId == 0) {

			restTemplate.postForEntity(crmRestUrl, thePedido, String.class);

			logger.info("in saveCustomer(): success POST");

		} else {

			restTemplate.put(crmRestUrl, thePedido);
			logger.info("in saveCustomer(): success PUT");
		}
	}

	@Override
	public List<Pedido> getPedidos() {

		logger.info("in getRelaciones(): Calling REST API " + crmRestUrl);

		ResponseEntity<List<Pedido>> responseEntity = restTemplate.exchange(crmRestUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Pedido>>() {
				});

		List<Pedido> pedido = responseEntity.getBody();

		logger.info("in getRelaciones(): Relacion" + pedido);

		return pedido;
	}

}
