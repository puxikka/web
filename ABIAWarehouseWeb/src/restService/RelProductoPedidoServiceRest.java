/** @file RelProductoPedidoServiceRest.java
 *  @brief RelProductoPedidoServiceRest
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package restService;
/** @brief Libraries
 */
import java.util.List;

import model.RelProductPedido;
/**
 * @brief interface RelProductoPedidoServiceRest
 */

public interface RelProductoPedidoServiceRest {
	
	/**
	 * @brief Method  to save a RelProductPedido
	 * @param rel  of RelProductPedido
	 */
	public void saveRel (RelProductPedido rel);
	
	/**
	 * @brief Method  to get all the RelProductPedido
	 * @return List<RelProductPedido>
	 */
	public List<RelProductPedido> getAllRelaciones();
}
