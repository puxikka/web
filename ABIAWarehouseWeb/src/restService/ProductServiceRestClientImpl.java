/** @file ProductServiceRestClientImpl.java
 *  @brief ProductServiceRestClientImpl
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package restService;

/** @brief Libraries
 */
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import model.Pedido;
import model.Product;

/**
 * @brief Class ProductServiceRestClientImpl
 */
@Service("productoRest")
public class ProductServiceRestClientImpl implements ProductServiceRest {
	/**
	 * @brief Attributes
	 */
	public ProductServiceRestClientImpl() {

	}

	private RestTemplate restTemplate;

	private String crmRestUrl;

	private Logger logger = Logger.getLogger(getClass().getName());

	@Autowired
	public ProductServiceRestClientImpl(RestTemplate theRestTemplate,
			@Value("http://localhost:8080/SpringMVCFormValidationPruebas/api/products") String theUrl) {
		restTemplate = theRestTemplate;
		crmRestUrl = theUrl;

		logger.info("Loaded property:  crm.rest.url=" + crmRestUrl);
	}

	@Override
	public List<Product> getProducts() {
		logger.info("in getCustomers(): Calling REST API " + crmRestUrl);

		ResponseEntity<List<Product>> responseEntity = restTemplate.exchange(crmRestUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Product>>() {
				});

		List<Product> produscts = responseEntity.getBody();

		logger.info("in getCustomers(): customers" + produscts);

		return produscts;
	}

	@Override
	public void saveProduct(Product theProduct) {
		logger.info("in saveCustomer(): Calling REST API " + crmRestUrl);

		int productId = theProduct.getIdProduct();

		if (productId == 0) {
			restTemplate.postForEntity(crmRestUrl, theProduct, String.class);

			logger.info("in saveCustomer(): success POST");

		} else {

			restTemplate.put(crmRestUrl, theProduct);
			logger.info("in saveCustomer(): success PUT");
		}

	}

	@Override
	public Product getProduct(int theId) {
		logger.info("in getProduct(): Calling REST API " + crmRestUrl);

		Product theProduct = restTemplate.getForObject(crmRestUrl + "/" + theId, Product.class);

		logger.info("in getProduct(): theProduct=" + theProduct);

		return theProduct;
	}

	@Override
	public void deleteProduct(int theId) {
		logger.info("in deleteCustomer(): Calling REST API " + crmRestUrl);

		restTemplate.delete(crmRestUrl + "/" + theId);

		logger.info("in deleteCustomer(): deleted customer theId=" + theId);

	}

	@Override
	public void saveProductWithJson(String jsonString) {
		logger.info("in saveCustomer(): Calling REST API " + crmRestUrl);

		restTemplate.postForEntity(crmRestUrl + "/json", jsonString, String.class);

		logger.info("in saveProduct(): success POST");

	}

	public void saveCatalogotWithJson(String jsonString) {
		logger.info("in saveCATALOGO(): Calling REST API " + crmRestUrl + "/catalogo/json");

		restTemplate.postForEntity(crmRestUrl + "/catalogo/json", jsonString, String.class);

		logger.info("in saveCatalogo(): success POST");

	}

	@Override
	public void updateProductWithJson(String jsonString) {
		logger.info("in saveCustomer(): Calling REST API " + crmRestUrl + "/update/json");

		restTemplate.put(crmRestUrl + "/update/json", jsonString);
		logger.info("in saveCustomer(): success PUT");

	}

}
