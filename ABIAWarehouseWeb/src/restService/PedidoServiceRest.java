/** @file PedidoServiceRest.java
 *  @brief PedidoServiceRest
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package restService;

import java.util.List;

import model.Pedido;

/**
 * @brief Interface PedidoServiceRest
 */
public interface PedidoServiceRest {
	/**
	 * @brief Method for saving a Pedido
	 * @param thePedido the Pedido
	 */
	public void savePedido (Pedido thePedido);
	/**
	 * @brief Method for getting all the Pedidos
	 * @return List<Pedido>
	 */
	public List<Pedido> getPedidos();
}
