/** @file ProductDAO.java
 *  @brief ProductDAO
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package dao
 */package dao;
 
 /** @brief Libraries
  */
import java.util.ArrayList;
import java.util.List;

import model.Pedido;
import model.Product;

/**
 * @brief Interface ProductDAO
 */
public interface ProductDAO {
	/**
	 * @brief Method for getting the Product with the specified id
	 * @param idProduct
	 * @return Product
	 */
	public Product getProductTypeById(int id);	
	/**
	 * @brief Method for getting all the products
	 * @return ArrayList<Product>
	 */
	public List<Product> getAllProducts();
	/**
	 * @brief Method for deletting the Product having the id
	 * @param id of Product
	 */
	public void deleteProduct(int id);
	/**
	 * @brief Method for saving the Product having the product
	 * @param product of Product
	 */
	public void saveProduct(Product product);
}
