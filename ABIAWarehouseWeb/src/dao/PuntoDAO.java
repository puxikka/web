/** @file Punto.java
 *  @brief Punto
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package dao
 */package dao;

/** @brief Libraries
  */
 import model.Punto;
/**
 * @brief Interface UserDAO
 */
public interface PuntoDAO {


	/**
	 * @brief Method for adding a punto
	 * @param punto a Punto
	 * @return boolean
	 */
	public boolean addPunto(Punto punto);
}
