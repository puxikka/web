/** @file UserDAO.java
 *  @brief UserDAO
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package dao
 */package dao;

 /** @brief Libraries
  */
import model.User;

/**
 * @brief Interface UserDAO
 */
public interface UserDAO {
	/**
	 * @brief Method for getting the user details with the username and password
	 * @param username username
	 * @param password password
	 * @return User
	 */
	public User getUserDetailsByUsernameAndPassword(String username,String password);
	
	/**
	 * @brief Method for adding a user
	 * @param user a User
	 * @return boolean
	 */
	public boolean addUser(User user);
	/**
	 * @brief Method getting 
	 * @param username a String
	 * @return User
	 */
	public User getUserWithName(String username);
}
