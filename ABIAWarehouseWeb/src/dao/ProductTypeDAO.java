/** @file ProductTypeDAO.java
 *  @brief ProductTypeDAO
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package dao
 */package dao;
 
 /** @brief Libraries
  */
import java.util.HashMap;
import java.util.List;

import model.ProductType;

/**
 * @brief Interface ProductTypeDAO
 */
public interface ProductTypeDAO {

	/**
	 * @brief Method for getting the ProductType with the id
	 * @param idProductType
	 * @return ProductType
	 */
	public ProductType getNameById(int id);
	/**
	 * @brief Method for getting a Map of the idProductType and the productType
	 * @return HadhMap<Integer, ProductType> 
	 */
	public HashMap<Integer, ProductType> getProductTypeMap();

}
