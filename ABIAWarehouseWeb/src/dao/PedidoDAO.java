/** @file PedidoDAO.java
 *  @brief PedidoDAO
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package dao;
/** @brief Libraries
 */
import java.util.List;
import model.Pedido;
import model.Product;
/**
 * @brief interface PedidoDAO
 */
public interface PedidoDAO {
	/**
	 * @brief Method for getting all the Pedidos
	 * @return List<Pedido>
	 */
	public List<Pedido> getAllPedidos();

	/**
	 * @brief Method adding a Pedido
	 * @param pedido the Pedido
	 */
	public void aņadirPedido(Pedido pedido);
}
