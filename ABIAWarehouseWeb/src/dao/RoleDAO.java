/** @file RoleDAO.java
 *  @brief RoleDAO
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package dao
 */package dao;
 
 /** @brief Libraries
  */
import model.Role;
import model.User;

/**
 * @brief Interface RoleDAO
 */
public interface RoleDAO {
	/**
	 * @brief Method for getting the Role by id
	 * @param id idRole
	 * @return Role
	 */
	public Role getRoleDetailsById(int id);
	
}
