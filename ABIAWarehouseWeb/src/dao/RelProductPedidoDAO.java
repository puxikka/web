/** @file RelProductPedidoDAO.java
 *  @brief RelProductPedidoDAO
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package dao
 */package dao;
 
 /** @brief Libraries
  */
import java.util.ArrayList;
import java.util.List;

import model.Pedido;
import model.Product;
import model.RelProductPedido;

/**
 * @brief Interface RelProductPedidoDAO
 */
public interface RelProductPedidoDAO {
	/**
	 * @brief Method for saving a Relation betwen Product and Pedido
	 * @param rel an RelProductPedido
	 */
	public void saveRelProductPedido(RelProductPedido rel);
	/**
	 * @brief Method for getting all the relation betwen Product and Pedido
	 * @return List<RelProductPedido>
	 */
	public List<RelProductPedido> getAllRel();
}
