/** @file ProductTypeDAOImpl.java
 *  @brief ProductTypeDAOImpl
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019*/
package dao.impl;

/** @brief Libraries
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.stereotype.Repository;

import dao.ProductTypeDAO;
import model.Product;
import model.ProductType;

/**
 * @brief Class ProductTypeDAOImpl
 */
@Repository
public class ProductTypeDAOImpl implements ProductTypeDAO {

	/**
	 * @brief Attributes
	 */
	@Autowired
	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public synchronized ProductType getNameById(int id) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ProductType.class);
		detachedCriteria.add(Restrictions.eq("ID_TIPO_PRODCUTO", id));
		List<ProductType> findByCriteria = (List<ProductType>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria.get(0);
		else
			return null;
	}


	@Override
	public synchronized HashMap<Integer, ProductType> getProductTypeMap() {
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ProductType.class);
		
		ArrayList<ProductType> findByCriteria = (ArrayList<ProductType>) hibernateTemplate.findByCriteria(detachedCriteria);
		HashMap<Integer, ProductType> productMap = new HashMap<Integer, ProductType>();
		for (ProductType pt : findByCriteria) {
		   productMap.put(pt.getIdTipoProduct(), pt);
		}
		return productMap;
	}

}
