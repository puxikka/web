/** @file PuntoDAOImpl.java
 *  @brief PuntoDAOImpl
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package dao.impl
 */package dao.impl;
 
 /** @brief Libraries
  */
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import dao.PuntoDAO;
import dao.UserDAO;
import model.Punto;
import model.User;

/**
 * @brief Class PuntoDAOImpl
 */
@Repository("puntoDAO")
public class PuntoDAOImpl implements PuntoDAO {

	/**
	 * @brief Attributes
	 */
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	@Override
	public synchronized boolean addPunto(Punto punto) {
		int id = (Integer)hibernateTemplate.save(punto);
		
		if(id>0)
			return true;
		return false;
	}
}	
