/** @file ProductDAOImpl.java
 *  @brief ProductDAOImpl
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019

/** @brief package dao.impl
 */
package dao.impl;

/** @brief Libraries
 */
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dao.ProductDAO;
import model.Pedido;
import model.Product;


/**
 * @brief Class ProductDAOImpl
 */
@Repository
public class ProductDAOImpl implements ProductDAO {

	/**
	 * @brief Attributes
	 */
	@Autowired
	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}


	@Override
	public synchronized Product getProductTypeById(int id) {

		Product findByCriteria = (Product) hibernateTemplate.get(Product.class, id);	
		return findByCriteria;

	}


	@Override
	public synchronized List<Product> getAllProducts() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Product.class);
		List<Product> findByCriteria = (List<Product>) hibernateTemplate.findByCriteria(detachedCriteria);
		return findByCriteria;
	}

	@Override
	public synchronized void deleteProduct(int id) {
		
		hibernateTemplate.bulkUpdate("delete from "+Product.class.getName()+" where ID_PRODUCT="+id);
		
	}

	@Override
	public synchronized void saveProduct(Product product) {

		if(product.getIdProduct()>0)
			hibernateTemplate.bulkUpdate("update "+Product.class.getName()+" set CANTIDAD_PRODUCTO = "
					+product.getCantidadProducto()+" where ID_PRODUCT= "+product.getIdProduct());
		else hibernateTemplate.saveOrUpdate(product);
		
	}

}
