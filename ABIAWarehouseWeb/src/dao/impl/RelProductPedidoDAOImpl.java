/** @file RelProductPedidoDAOImpl.java
 *  @brief RelProductPedidoDAOImpl
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */

/** @brief package dao.impl
 */
package dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
/** @brief Libraries
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import dao.RelProductPedidoDAO;
import model.Product;
import model.RelProductPedido;


/**
 * @brief Class RelProductPedidoDAOImpl
 */
@Repository
public class RelProductPedidoDAOImpl implements RelProductPedidoDAO {

	/**
	 * @brief Attributes
	 */
	@Autowired
	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}


	@Override
	public synchronized void saveRelProductPedido(RelProductPedido rel) {
		hibernateTemplate.save(rel);
		
	}

	@Override
	public synchronized List<RelProductPedido> getAllRel() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(RelProductPedido.class);
		List<RelProductPedido> findByCriteria = (List<RelProductPedido>) hibernateTemplate.findByCriteria(detachedCriteria);
		return findByCriteria;
		
	}

}
