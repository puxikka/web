/** @file PedidoDAOImpl.java
 *  @brief PedidoDAOImpl
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */package dao.impl;
 /** @brief Libraries
  */
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import dao.PedidoDAO;
import model.Pedido;
import model.Product;
/**
 * @brief Class PedidoDAOImpl
 */
@Repository
public class PedidoDAOImpl implements PedidoDAO{

	/**
	 * @brief Attributes
	 */
	@Autowired
	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}


	@Override
	 public synchronized List<Pedido>  getAllPedidos() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Pedido.class);
		List<Pedido> findByCriteria = (List<Pedido>) hibernateTemplate.findByCriteria(detachedCriteria);
		return findByCriteria;
	}

	@Override
	public synchronized void aņadirPedido(Pedido pedido) {

		hibernateTemplate.saveOrUpdate(pedido);
		
	}


}
