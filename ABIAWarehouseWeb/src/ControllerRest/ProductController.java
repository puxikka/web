/** @file ProductController.java
 *  @brief ProductController
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package ControllerRest;

/** @brief Libraries
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.google.gson.Gson;
import Validator.ValidationUtils;
import model.Pedido;
import model.Product;
import model.RelProductPedido;
import schemas.Schemas;
import service.PedidoService;
import service.ProductService;
import service.RelProductPedidoService;
/**
 * @brief Class ProductController
 */
@RestController
@RequestMapping("/api")

public class ProductController {
	/**
	 * @brief Attributes
	 */
	@Autowired
	private ProductService productService;
	@Autowired
	private PedidoService pedidoService;
	@Autowired
	private RelProductPedidoService relService;

	// add mapping for GET /productos
	@GetMapping(value = "/products")
	public List<Product> getProducts() {

		return productService.getProducts();

	}

	// add mapping for GET /productos
	@GetMapping("/pedidos")
	public List<Pedido> getPedidos() {

		return pedidoService.getPedidos();

	}
	
	@GetMapping("/products/{id}")
	public Product getProducts(@PathVariable int id) {
		
		Product p = productService.validateProduct(id);

		return p;
	}
	

	// add mapping for GET /relaciones
	@GetMapping("/rel")
	public List<RelProductPedido> getRelaciones() {

		return relService.getRels();

	}

	@PostMapping("/pedidos")
	public Pedido addPedido(@RequestBody Pedido thePedido) {

		thePedido.setIdPedidos(0);

		pedidoService.addPedido(thePedido);

		return thePedido;
	}

	@PostMapping("/rel")
	public RelProductPedido addRel(@RequestBody RelProductPedido rel) {

		relService.saveRelProductPedido(rel);

		return rel;
	}

	@PostMapping("/products/json")
	public Product addProductwithJson(@RequestBody String jsonString) throws ProcessingException, IOException {

		Gson g = new Gson();
		Product p = g.fromJson(jsonString, Product.class);
		p.setIdProduct(0);

		Schemas schema = new Schemas();

		if (ValidationUtils.isJsonValid(schema.getAddProductSchema(), jsonString)) {
			System.out.println("Valid!");
			productService.saveProduct(p);

		}

		return p;

	}

	@PostMapping("/products/catalogo/json")
	public List<Product> addCatalogowithJson(@RequestBody String jsonString) throws ProcessingException, IOException {

		Schemas schema = new Schemas();
		List<Product> listaActualizada = new ArrayList<Product>();
		if (ValidationUtils.isJsonValid(schema.getAddCatalogoSchema(), jsonString)) {

			JSONArray jsonArr = new JSONArray(jsonString);

			for (int i = 0; i < jsonArr.length(); i++) {
				JSONObject jsonObj = jsonArr.getJSONObject(i);

				Product p = new Product();

				p.setCantidadProducto(jsonObj.getInt("cantidadProducto"));
				p.setIdProduct(0);
				p.setNombreProducto(jsonObj.getString("nombreProducto"));
				p.setPrecioUnidad(jsonObj.getInt("precioUnidad"));
				p.setTipoProducto(jsonObj.getInt("tipoProducto"));
				p.setUsuarioId(jsonObj.getInt("usuarioId"));
				listaActualizada.add(p);
			}

			for (Product p : listaActualizada) {

				productService.saveProduct(p);
			}
		}

		return listaActualizada;

	}

	@PostMapping("/products")
	public Product addProduct(@RequestBody Product theProduct) {

		theProduct.setIdProduct(0);

		productService.saveProduct(theProduct);

		return theProduct;
	}

	// add mapping for PUT /customers - update existing customer

	@PutMapping("/products")
	public Product updateProduct(@RequestBody Product theCustomer) {

		productService.saveProduct(theCustomer);

		return theCustomer;

	}

	@PutMapping("/products/update/json")
	public Product updateProductwithJson(@RequestBody String jsonString) throws ProcessingException, IOException {

		Gson g = new Gson();
		Product p = g.fromJson(jsonString, Product.class);

		Schemas schema = new Schemas();

		if (ValidationUtils.isJsonValid(schema.getUpdateProductSchema(), jsonString)) {
			System.out.println("Valid Schema!");
			productService.saveProduct(p);

		}

		return p;

	}

	// add mapping for DELETE /products/{productId}

	@DeleteMapping("/products/{productId}")
	public String deleteProduct(@PathVariable int productId) {

		Product tempProduc = productService.validateProduct(productId);

		if (tempProduc == null) {
			System.out.println("no se ha encontrado: " + productId);
		}

		productService.deleteProduct(productId);

		return "Deleted product id - " + productId;
	}
}
