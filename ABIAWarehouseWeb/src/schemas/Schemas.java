/** @file Schemas.java
 *  @brief Schemas
 *  @authors
 *  Name          | Surname         | Email                                |
 *  ------------- | -------------- | ------------------------------------ |
 *  Borja	      | Garcia          | borja.garcia@alumni.mondragon.edu     |
 *  Ander	      | Recalde          | ander.recalde@alumni.mondragon.edu     |
 *  Manex	      | Bengoa          | manex.bengoa@alumni.mondragon.edu     |
 *  Sergio	      | Rios          | segio.rios@alumni.mondragon.edu     |
 *  @date 08/06/2019
 */
package schemas;
/**
 * @brief Class Schemas
 */
public class Schemas {
	/**
	 * @brief Attributes
	 */
	String addProductSchema = "{\r\n" + 
			"  \"$schema\": \"http://json-schema.org/draft-04/schema#\",\r\n" + 
			"  \"type\": \"object\",\r\n" + 
			"  \"properties\": {\r\n" + 
			"    \"tipoProducto\": {\r\n" + 
			"      \"type\": \"integer\",\r\n" + 
			"		\"maximum\": 4\r\n" + 
			"    },\r\n" + 
			"    \"usuarioId\": {\r\n" + 
			"      \"type\": \"integer\"\r\n" + 
			"    },\r\n" + 
			"    \"nombreProducto\": {\r\n" + 
			"      \"type\": \"string\"\r\n" + 
			"    },\r\n" + 
			"    \"cantidadProducto\": {\r\n" + 
			"      \"type\": \"integer\",\r\n" + 
			"      \"minimum\": 0\r\n" + 
			"    },\r\n" + 
			"    \"precioUnidad\": {\r\n" + 
			"      \"type\": \"integer\",\r\n" + 
			"      \"minimum\": 0\r\n" + 
			"    }\r\n" + 
			"  },\r\n" + 
			"  \"required\": [\r\n" + 
			"    \"tipoProducto\",\r\n" + 
			"    \"usuarioId\",\r\n" + 
			"    \"nombreProducto\",\r\n" + 
			"    \"cantidadProducto\",\r\n" + 
			"    \"precioUnidad\"\r\n" + 
			"  ]\r\n" + 
			"}";
	String addCatalogoSchema = "{\r\n" + 
			"  \"type\": \"array\",\r\n" + 
			"  \"items\": {\r\n" + 
			"    \"type\": \"object\",\r\n" + 
			"    \"properties\": {\r\n" + 
			"      \"tipoProducto\": {\r\n" + 
			"          \"type\": \"integer\",\r\n" + 
			"		  \"maximum\": 4\r\n" + 
			"        },\r\n" + 
			"        \"precioUnidad\": {\r\n" + 
			"          \"type\": \"integer\",\r\n" + 
			"		  \"minimum\": 0\r\n" + 
			"        },\r\n" + 
			"        \"cantidadProducto\": {\r\n" + 
			"          \"type\": \"integer\",\r\n" + 
			"		  \"minimum\": 0\r\n" + 
			"        },\r\n" + 
			"        \"nombreProducto\": {\r\n" + 
			"          \"type\": \"string\"\r\n" + 
			"        }\r\n" + 
			"      },\r\n" + 
			"      \"required\": [\r\n" + 
			"        \"tipoProducto\",\r\n" + 
			"        \"precioUnidad\",\r\n" + 
			"        \"cantidadProducto\",\r\n" + 
			"        \"nombreProducto\"\r\n" + 
			"    ]\r\n" + 
			"  }\r\n" + 
			"}";
	String comprarProductoSchema = "{\r\n" + 
			"  \"type\": \"array\",\r\n" + 
			"  \"items\": {\r\n" + 
			"    \"type\": \"object\",\r\n" + 
			"    \"properties\": {\r\n" + 
			"		\"idProduct\": {\r\n" + 
			"          \"type\": \"integer\"\r\n" + 
			"        },\r\n" + 
			"		\"tipoProducto\": {\r\n" + 
			"          \"type\": \"integer\",\r\n" + 
			"		  \"maximum\": 4\r\n" + 
			"        },\r\n" + 
			"        \"precioUnidad\": {\r\n" + 
			"          \"type\": \"integer\",\r\n" + 
			"		  \"minimum\": 0\r\n" + 
			"        },\r\n" + 
			"        \"usuarioId\": {\r\n" + 
			"          \"type\": \"integer\"\r\n" + 
			"        },\r\n" + 
			"        \"cantidadProducto\": {\r\n" + 
			"          \"type\": \"integer\",\r\n" + 
			"		  \"minimum\": 0\r\n" + 
			"        },\r\n" + 
			"        \"nombreProducto\": {\r\n" + 
			"          \"type\": \"string\"\r\n" + 
			"        }\r\n" + 
			"      },\r\n" + 
			"      \"required\": [\r\n" + 
			"		\"idProduct\",\r\n" + 
			"        \"tipoProducto\",\r\n" + 
			"        \"precioUnidad\",\r\n" + 
			"        \"usuarioId\",\r\n" + 
			"        \"cantidadProducto\",\r\n" + 
			"        \"nombreProducto\"\r\n" + 
			"    ]\r\n" + 
			"  }\r\n" + 
			"}";
	String editarProductoSchema = "{\r\n" + 
			"  \"type\": \"array\",\r\n" + 
			"  \"items\": {\r\n" + 
			"    \"type\": \"object\",\r\n" + 
			"    \"properties\": {\r\n" + 
			"		\"idProduct\": {\r\n" + 
			"          \"type\": \"integer\"\r\n" + 
			"        },\r\n" + 
			"		\"tipoProducto\": {\r\n" + 
			"          \"type\": \"integer\",\r\n" + 
			"		  \"maximum\": 4\r\n" + 
			"        },\r\n" + 
			"        \"precioUnidad\": {\r\n" + 
			"          \"type\": \"integer\",\r\n" + 
			"		  \"minimum\": 0\r\n" + 
			"        },\r\n" + 
			"        \"usuarioId\": {\r\n" + 
			"          \"type\": \"integer\"\r\n" + 
			"        },\r\n" + 
			"        \"cantidadProducto\": {\r\n" + 
			"          \"type\": \"integer\",\r\n" + 
			"		  \"minimum\": 0\r\n" + 
			"        },\r\n" + 
			"        \"nombreProducto\": {\r\n" + 
			"          \"type\": \"string\"\r\n" + 
			"        }\r\n" + 
			"      },\r\n" + 
			"      \"required\": [\r\n" + 
			"		\"idProduct\",\r\n" + 
			"        \"tipoProducto\",\r\n" + 
			"        \"precioUnidad\",\r\n" + 
			"        \"usuarioId\",\r\n" + 
			"        \"cantidadProducto\",\r\n" + 
			"        \"nombreProducto\"\r\n" + 
			"    ]\r\n" + 
			"  }\r\n" + 
			"}";
	String updateProductSchema = "{\r\n" + 
			"  \"$schema\": \"http://json-schema.org/draft-04/schema#\",\r\n" + 
			"  \"type\": \"object\",\r\n" + 
			"  \"properties\": {\r\n" +
			"    \"idProduct\": {\r\n" + 
			"      \"type\": \"integer\"\r\n" + 
			"    },\r\n" + 
			"    \"tipoProducto\": {\r\n" + 
			"      \"type\": \"integer\",\r\n" + 
			"		\"maximum\": 4\r\n" + 
			"    },\r\n" + 
			"    \"usuarioId\": {\r\n" + 
			"      \"type\": \"integer\"\r\n" + 
			"    },\r\n" + 
			"    \"nombreProducto\": {\r\n" + 
			"      \"type\": \"string\"\r\n" + 
			"    },\r\n" + 
			"    \"cantidadProducto\": {\r\n" + 
			"      \"type\": \"integer\",\r\n" + 
			"      \"minimum\": 0\r\n" + 
			"    },\r\n" + 
			"    \"precioUnidad\": {\r\n" + 
			"      \"type\": \"integer\",\r\n" + 
			"      \"minimum\": 0\r\n" + 
			"    }\r\n" + 
			"  },\r\n" + 
			"  \"required\": [\r\n" + 
			"    \"tipoProducto\",\r\n" + 
			"    \"usuarioId\",\r\n" + 
			"    \"nombreProducto\",\r\n" + 
			"    \"cantidadProducto\",\r\n" + 
			"    \"precioUnidad\"\r\n" + 
			"  ]\r\n" + 
			"}";		
	/**
	 * @brief Method for get the value schema for updating Products
	 * @return String
	 */
	public String getUpdateProductSchema() {
		return updateProductSchema;
	}
	/**
	 * @brief Method for get the value schema for editing Products
	 * @return String
	 */

	public String getEditarProductoSchema() {
		return editarProductoSchema;
	}
	/**
	 * @brief Method for get the value schema for buying Products
	 * @return String
	 */
	public String getComprarProductoSchema() {
		return comprarProductoSchema;
	}
	/**
	 * @brief Method for get the value schema for  adding a catalog of Products
	 * @return String
	 */
	public String getAddCatalogoSchema() {
		return addCatalogoSchema;
	}
	/**
	 * @brief Method for get the value schema for adding Products
	 * @return String
	 */
	public String getAddProductSchema() {
		return addProductSchema;
	}
}
