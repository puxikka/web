<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<form:form action="/SpringMVCFormValidationPruebas/registerSuccess"
	method="post" class="text-center p-5">

	<!-- Role -->
	
	<p class="h4 mb-4">Register</p>
	<!-- Username -->
	<input name="name" type="text"
		id="defaultRegisterFormFirstName" class="form-control  mb-4"
		placeholder="Username" />
		
	<input name="nombre_usuario" type="text" id="defaultRegisterFormName"
		class="form-control mb-4" placeholder="nombre_usuario" />
	
	<!-- Password -->
	<input name="password" type="password"
		id="defaultRegisterFormPassword" class="form-control  mb-4"
		placeholder="Password"
		aria-describedby="defaultRegisterFormPasswordHelpBlock" />
	
	<select name="idRole" class="browser-default custom-select mb-4">
		<option value="" disabled selected>Select Role</option>
		<option value="2">Cliente</option>
		<option value="3">Tienda</option>	
	</select>
	<!-- Punto -->
	<input name="posX" type="number" class="form-control  mb-4" placeholder="posX" />	
	<input name="posY" type="number" class="form-control  mb-4" placeholder="posY" />	
	<input name="direccion" type="text" class="form-control  mb-4" placeholder="direccion" />	
	
	
	<!-- Sign up button -->
	<button class="btn btn-info my-4 btn-block peach-gradient"
		type="submit" value="Confirm">Sign up</button>

</form:form>
