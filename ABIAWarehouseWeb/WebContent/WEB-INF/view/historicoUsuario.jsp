<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="page-wrapper">
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Table of Products</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Products</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover"
								id="dataTables-example">
								<thead>
									<tr>
										<th>Id Pedido</th>
										<th>Estado</th>
										<th>Fecha</th>
										<th>Precio Total</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${pedidos}" var="pedido">
										
											<tr class="odd gradeX">
												<td><c:out value="${pedido.idPedidos}" /></td>
												<td><c:out value="${pedido.idEstado}" /></td>
												<td><c:out value="${pedido.fechaPedido}" /></td>
												<td><c:out value="${pedido.precioTotal}" /></td>
											</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
	</div>
		
</div>	

