<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Aņadir Catalogo</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Introduce Json</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-6">
									
										<form:form action="aņadirCatalogo" modelAttribute="jsonString"
										method="POST">									
												<h2>JSON:</h2>
												
												<form:textarea path="mensaje" rows="20" cols="80" value=" " />						
											<p>
												<button type="submit" value="Submit" class="btn btn-success">Save</button>
											</p>
										</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>