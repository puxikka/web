<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String products = (String) request.getAttribute("productos");
%>



<div>
<form:form action="/SpringMVCFormValidationPruebas/createCesta"
	method="post" class="text-center p-5">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Table of Products</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Products</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover"
								id="dataTables-example">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Tipo producto</th>
										<th>Cantidad Disp.</th>
										<th>Precio por Unidades</th>
										<th>A�adir Cantidad</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${products}" var="product">

										<tr class="odd gradeX">
											<td><c:out value="${product.nombreProducto}" /></td>
											<td><c:choose>
													<c:when test="${product.tipoProducto=='0'}">
										   						Carne
										   					</c:when>
													<c:when test="${product.tipoProducto=='1'}">
										   						Pescado
										   					</c:when>
													<c:when test="${product.tipoProducto=='2'}">
										   						Verdura
										   					</c:when>
													<c:when test="${product.tipoProducto=='3'}">
										   						Panader�a
										   					</c:when>
													<c:when test="${product.tipoProducto=='4'}">
										   						Otros
										   					</c:when>
												</c:choose></td>
											<td><c:out value="${product.cantidadProducto}" /></td>
											<td><c:out value="${product.precioUnidad}" /></td>
											<td><input type="number" min="0"
												max="${product.cantidadProducto}" value=0 name="cantidad"></input>

											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
	</div>
	<input type="hidden" id="YourHiddenID" value="" name="cestas">
	<button class="btn btn-info btn-block my-4 peach-gradient"
		type="submit" onclick="myFunction()">Crear Cesta</button>
	<!-- /.container-fluid -->
</form:form>
</div>
<!--<link href="resources/admin/css/bootstrap.min.css" rel="stylesheet">

<link href="resources/admin/css/metisMenu.min.css" rel="stylesheet">

<link href="resources/admin/css/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">

<link href="resources/admin/css/dataTables/dataTables.responsive.css"
	rel="stylesheet"> 


<link href="resources/admin/css/startmin.css" rel="stylesheet">

<link href="resources/admin/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">
<script src="resources/admin/js/jquery2.min.js"></script>


<script src="resources/admin/js/bootstrap.min.js"></script>

<script src="resources/admin/js/metisMenu.min.js"></script>

<script src="resources/admin/js/dataTables/jquery.dataTables.min.js"></script>
<script src="resources/admin/js/dataTables/dataTables.bootstrap.min.js"></script>

<script src="resources/admin/js/startmin.js"></script>-->

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive : true
		});
	});
</script>
<script>
	function myFunction() {

		var array = document.getElementsByName("cantidad");
		var json =
<%=products%>
	for (var i = 0; i < array.length; i++) {
			json[i].cantidadProducto = array[i].value;
		}

		var myJSON = JSON.stringify(json);
		document.getElementById("YourHiddenID").value = myJSON;

	}
</script>