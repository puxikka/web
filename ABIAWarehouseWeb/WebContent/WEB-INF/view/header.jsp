<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a href="/SpringMVCFormValidationPruebas/"><img
		src="resources/img/PUXIKKA_NOFONDO.png" height="36" width="36"
		alt="ABIAWarehouse logo"></a> &emsp;
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<c:if test="${empty user}">
				<li class="nav-item"><a class="nav-link"
					href="/SpringMVCFormValidationPruebas/login">Login</a></li>
			</c:if>
			<!--<li class="nav-item"><a class="nav-link"
				href="/SpringMVCFormValidationPruebas/register">Register</a></li>-->
			<c:if test="${not empty user}">
				<c:choose>
					<c:when test="${user.idRole==2}">
<!-- Cliente -->
						<li class="nav-item"><a class="nav-link"
							href="/SpringMVCFormValidationPruebas/verProductos">Ver productos</a></li>
						<li class="nav-item"><a class="nav-link"
							href="/SpringMVCFormValidationPruebas/historicoUsuario">Hist�rico de pedidos</a></li>
								
					
					</c:when>
					<c:when test="${user.idRole==3}">
<!-- Tienda -->

						<li class="nav-item"><a class="nav-link"
							href="/SpringMVCFormValidationPruebas/orderHistoryAll">Lista Productos</a></li>
						<li class="nav-item"><a class="nav-link"
							href="/SpringMVCFormValidationPruebas/logistic">A�adir Producto</a></li>
						<li class="nav-item"><a class="nav-link"
							href="/SpringMVCFormValidationPruebas/workstationInfo">A�adir Cat�logo</a></li>
					</c:when>
					<c:when test="${user.idRole==4}">
						<li class="nav-item"><a class="nav-link"
							href="/SpringMVCFormValidationPruebas/orderHistoryAll">Hola Admin</a></li>
					</c:when>
			
				</c:choose>
				<c:if test="${user.idRole!=0}">
					<li id="logOffItem" class="nav-item"><a class="nav-link"
					href="/SpringMVCFormValidationPruebas/logOff">Log Off</a></li>
				</c:if>
			</c:if>

		</ul>
	</div>
</nav>

