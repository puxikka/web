<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">A�adir Product</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">introduce los valores</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-6">
									
										<form:form action="vehicleInfo" modelAttribute="product"
										method="POST" id="form1">

										<div class="form-group">
											<label>Name:</label>
											<form:input type="hidden" value= "0"  class="form-control" path="idProduct" />
											<form:input type="text" value="" maxlength="12" class="form-control" path="nombreProducto" />
											
											<label>Tipo Producto:</label>
											<select name="tipoProducto" class="browser-default custom-select mb-4">
												<option value="5" selected>Select Role</option>
												<option value="0">Carne</option>
												<option value="1">Pescado</option>	
												<option value="2">Verdura</option>	
												<option value="3">Panaderia</option>	
												<option value="4">Otros</option>	
											</select>
											
											<label>Cantidad:</label>
											<form:input type="number" min="0" class="form-control" path="cantidadProducto" value=""/>
											
											<label>Precio:</label>
											<form:input type="number" min="0" class="form-control" path="precioUnidad" value=""/>
					
										</div>
										
										<p>
												<button type="submit" value="Submit"
													class="btn btn-success">Save</button>
										</p>
										</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>