	<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="page-wrapper">
	<% String products = (String) request.getAttribute("productos"); %>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Mis Productos</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Productos</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover"
								id="dataTables-example">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Tipo producto</th>
										<th>Cantidad Disp.</th>
										<th>Precio por Unidades</th>
										<th>A�adir Cantidad</th>
										<th>Eliminar</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${products}" var="product">
										
											<tr class="odd gradeX">
												<td><c:out value="${product.nombreProducto}" /></td>
												<td>										
												<c:choose>
															<c:when test="${product.tipoProducto=='0'}">
										   						Carne
										   					</c:when>
															<c:when test="${product.tipoProducto=='1'}">
										   						Pescado
										   					</c:when>
															<c:when test="${product.tipoProducto=='2'}">
										   						Verdura
										   					</c:when>
										   					<c:when test="${product.tipoProducto=='3'}">
										   						Panader�a
										   					</c:when>
										   					<c:when test="${product.tipoProducto=='4'}">
										   						Otros
										   					</c:when>
												</c:choose>		
												
												</td>
												<td><c:out value="${product.cantidadProducto}" /></td>
												<td><c:out value="${product.precioUnidad}" /></td>
												<td>
								
													<input type="number"  
													min= 0 value="${product.cantidadProducto}" name="cantidad"></input>
								
												</td>
												<td>
												<button onclick="deleteRow(this)" class="btn btn-info btn-block my-4 peach-gradient"
												 type="submit" name="idProducto" value="${product.idProduct}">X</button>
												
												
											</td>
											</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
	</div>
	<form:form id ="form" action="/SpringMVCFormValidationPruebas/eliminarOrderHistoryAll" method="post">
	
		<input type="hidden" name="cesta" id="cesta" value="" />
		
	</form:form>
	<form:form id ="form" action="/SpringMVCFormValidationPruebas/saveOrderHistoryAll" method="post">
		<input type="hidden" id = "YourHiddenID" value="" name="cesta" >  
		<button name="idProducto" class="btn btn-info btn-block my-4 peach-gradient" type="submit"  onclick="enviarJsonEdit()">Guardar Productos</button>
	
	</form:form>
</div>	

<script>
function enviarJsonEdit() {

	var array = document.getElementsByName("cantidad");
	var json = <%= products%>
	
	for (var i=0; i<array.length; i++)
	{ 
		
		json[i].cantidadProducto = Number(array[i].value);
	}

	var myJSON = JSON.stringify(json);
	document.getElementById("YourHiddenID").value = myJSON;

	console.log(myJSON);
}

function deleteRow(o) {

	var p=o.parentNode.parentNode;

    var array = document.getElementsByName("idProducto");
    var json = <%= products%>
    console.log(array.length);
    for (var i=0; i<array.length-1; i++)
	{ 	
		if(json[i].idProduct == (o.value)){
			json[i].nombreProducto = "eliminar";
			 console.log(json[i].nombreProducto);
		}
		
	}

    p.parentNode.removeChild(p);
    console.log(json);
    var myJSON = JSON.stringify(json);
    document.getElementById("cesta").value = myJSON;
    console.log(document.getElementById("cesta").value);
    form = document.getElementById("form");
    form.submit();
}
</script>